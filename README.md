# Heartbleed Vulnrability CCSEP 2021

## About

This repo contains a server that is vulnrable to the Heartbleed attack and the
associated attacking script and detection script. There is a branch of this repo
where the vulnrability is patched and can be tested accordingly.

Heartbleed is a buffer overread exploit that is present in an early version of OpenSSL, this was first found in 2014 and patched in a matter of days. This repo is designed do demonstrate this vulnrability and is not to be used for malicious purposes. All credit for the attack and detection script go to the original authors stated in their respective README. All of the programs have a seperate docker container to allow for anyone with docker installed on their system to run these programs. Instructions on how to install docker on your particular system can be found at https://docs.docker.com/engine/install/ .

## Patch

For the patched version of the server checkout the OpenSSLPatch branch.
The vulnrable version will be kept in master.

## Server

The server folder contains the Dockerfile and html for a simple server + webpage that is vulnrable to the attack.
Instructions on how to run this are in the directory's README.

## Detector

This directory contains a Dockfile that will download a github repo that contains the detection program and build it.
Intstructions on how to build and run are contained in the directory's README.

## Attack

This directory contains the python script and Dockerfile to run the python script. This script will perform the buffer overread on the server.
Instructions on how to use are contained in the directory's README.

## Attribution

List of tasks performed by each group member in this repo, attribution for the
presentation can be found in the presentation.

|Task|Attribution|
|----|-----------|
|Hearbleed Server|Benjamin Bowers|
|Banking Webpage|Deckard Gerritsen|
|Heartbleed Attack Container|Benjamin Bowers|
|Heartbleed Attack README|Dylan Chanter|
|Heartbleed Detection|Benjamin Bowers|
|Repo Structure and setup & README|Benjamin Bowers|
|Heartbleed Vulnrability Patch|Dylan Chanter|
