# Heartbleed server
Simple apache server with a banking app webpage. Make sure to use https:// when testing the exploit as the overead only works on that.
The ip address of the server will be printed to console when run.
## Build
Run this command in bash
```
make build
```

## Run
Run this command in bash
```
make run
```

## How to stop the server
Run this command in bash
```
docker kill heartbleed-server
```
