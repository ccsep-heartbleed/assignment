# Heartbleed Detector
Simple detection program written in go take from https://github.com/FiloSottile/Heartbleed all credit goes to the author
of that code.
## Build

Build with
```
./build.sh
```

## Run

Run with
```
./run.sh {ip address}
```

